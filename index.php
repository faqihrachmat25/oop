<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
		require_once 'animal.php';

		$Shaun = new Animal("Shaun");
		echo "Name : ".$Shaun->name."<br>";
		echo "Legs : ".$Shaun->legs."<br>";
		echo "Cold Blooded : ".$Shaun->cold_blooded."<br>";
		echo "<br><br>";

		require_once 'frog.php';
		$kodok = new Frog("buduk");
		echo "Name : ".$kodok->name."<br>";
		echo "Legs : ".$kodok->legs."<br>";
		echo "Cold Blooded : ".$kodok->cold_blooded."<br>";
		echo "Jump : ".$kodok->jump()."<br>";
		echo "<br><br>";

		require_once 'ape.php';

		$Sungokong = new Ape("Kera Sakti");
		echo "Name : ".$Sungokong->name."<br>";
		echo "Legs : ".$Sungokong->legs."<br>";
		echo "Cold Blooded : ".$Sungokong->cold_blooded."<br>";
		echo "Yell : ".$Sungokong->yell();
		echo "<br><br>";
	?>
</body>
</html>